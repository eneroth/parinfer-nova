// Load WASM
// ##################################


// Construct path to WASM
const wasmLoc = "../wasm/parinfer_rust.wasm";
const apath = nova.path.normalize(nova.path.join(__dirname, wasmLoc));

console.log("Loading WASM from " + apath);


// Read WASM
const afile = nova.fs.open(apath, "rb");
const acontent = afile.read();


// Load and instantiate WASM
if (WebAssembly.validate(acontent) !== true) {
  throw Error("ERROR, invalid WASM!");
}

const mod = new WebAssembly.Module(acontent);
const parinferRust = new WebAssembly.Instance(mod, {});
const wasm = parinferRust.exports;


// WASM I/O
// ##################################
let WASM_VECTOR_LEN = 0;

let cachegetUint8Memory0 = null;
function getUint8Memory0() {
    if (cachegetUint8Memory0 === null || cachegetUint8Memory0.buffer !== wasm.memory.buffer) {
        cachegetUint8Memory0 = new Uint8Array(wasm.memory.buffer);
    }
    return cachegetUint8Memory0;
}

const lTextEncoder = typeof TextEncoder === 'undefined' ? (0, module.require)('util').TextEncoder : TextEncoder;

let cachedTextEncoder = new lTextEncoder('utf-8');

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});


function passStringToWasm0(arg, malloc, realloc) {

    if (realloc === undefined) {
        const buf = cachedTextEncoder.encode(arg);
        const ptr = malloc(buf.length);
        getUint8Memory0().subarray(ptr, ptr + buf.length).set(buf);
        WASM_VECTOR_LEN = buf.length;
        return ptr;
    }

    let len = arg.length;
    let ptr = malloc(len);

    const mem = getUint8Memory0();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = realloc(ptr, len, len = offset + arg.length * 3);
        const view = getUint8Memory0().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}


let cachegetInt32Memory0 = null;
function getInt32Memory0() {
    if (cachegetInt32Memory0 === null || cachegetInt32Memory0.buffer !== wasm.memory.buffer) {
        cachegetInt32Memory0 = new Int32Array(wasm.memory.buffer);
    }
    return cachegetInt32Memory0;
}


function runParinfer(input) {
    try {
        const retptr = wasm.__wbindgen_export_0.value - 16;
        wasm.__wbindgen_export_0.value = retptr;
        var ptr0 = passStringToWasm0(input, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len0 = WASM_VECTOR_LEN;
        wasm.run_parinfer(retptr, ptr0, len0);
        var r0 = getInt32Memory0()[retptr / 4 + 0];
        var r1 = getInt32Memory0()[retptr / 4 + 1];
        var retArr = getUint8Memory0().subarray(r0, r0 + r1);
        return String.fromCharCode.apply(null, retArr);
    } finally {
        wasm.__wbindgen_export_0.value += 16;
        wasm.__wbindgen_free(r0, r1);
    }
}


// Parinfer runner
// ##################################
function mode(mode) {
  return function(text, options) {
    let config = {
      mode: mode,
      text: text,
      options: options
    };
  
    let configJsonString = JSON.stringify(config);
    let jsonStringRes = runParinfer(configJsonString);
    let jsonRes = JSON.parse(jsonStringRes);

    console.log("Config to Parinfer");
    console.log(configJsonString);
    console.log("Parinfer result");
    console.log(jsonStringRes);
    
    if (jsonRes.success !== true) {
      console.log("ERROR *******************************************************");
      console.log("Successful? " + jsonRes.success);
      console.log("Error: " + jsonRes.error);
      console.log("Config:");
      console.log(configJsonString);
      console.log("Result:");
      console.log(JSON.stringify(jsonStringRes));
      console.log("ERROR **********************************************************");
    }
    
    return jsonRes;
  };
}

let indentMode = mode('indent');
let parenMode = mode('paren');
let smartMode = mode('smart');


// Nova integration
// ##################################
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

var editors = {};


function getColAndCursorX(selectRange, documentEol, documentText) {
  const eolLength = documentEol.length;
  const textLines = documentText.split(documentEol);
  
  var lineCount = 0;
  var currentPos = selectRange.start;
  var oldCurrentPos = currentPos;
  var lineCharCount = -eolLength;
  
  do {
    lineCharCount = textLines[lineCount].length + eolLength;
    oldCurrentPos = currentPos;
    currentPos -= lineCharCount;
    lineCount += 1;
    
  } while(oldCurrentPos >= lineCharCount)
  
  return {
    cursorX: oldCurrentPos,
    lineNumber: lineCount -1
  }
}

function colAndCursorXtoPos(lineNumber, cursorX, documentEol, documentText) {
  const eolLength = documentEol.length;
  const textLines = documentText.split(documentEol);
  
  console.log("lineNumber: " + lineNumber);
  
  var totalCount = 0;
  var lineCount = 0;
  
  for (i = 0; i < lineNumber; i++) {
    lineCount++;
    totalCount += (textLines[i].length);
  }
  
  console.log("totalCount: " + totalCount + " cursorX: " + cursorX + " lineCount: " + lineCount + " eolLength: " + eolLength);
  resultCount = totalCount + cursorX + lineCount * eolLength;
  console.log("New cursor pos: " + resultCount);
  return resultCount;
}


function getDocumentTextAndRange(selectRange, document) {
  let documentTextRange = new Range(0, document.length);
  let documentText = document.getTextInRange(documentTextRange);
  
  let { cursorX, lineNumber } = getColAndCursorX(selectRange, document.eol, documentText);
  
  return {
    range: documentTextRange,
    text: documentText,
    cursorX: cursorX,
    lineNumber: lineNumber
  }
}


function runParinferOnDocument(parinferMode, editor, editorId) {
  console.log("Editors");
  console.log(JSON.stringify(editors));
  
  let document = editor.document;
  
  let parinferFunc = mode(parinferMode);
  let { range, text, cursorX, lineNumber } = getDocumentTextAndRange(editor.selectedRange, document); 
  
  let options = { 
    prevText: editors[editorId]["text"],
    prevCursorLine: editors[editorId]["lineNumber"],
    cursorLine: lineNumber,
    prevCursorX: editors[editorId]["cursorX"],
    cursorX: cursorX
  };
  
  editors[editorId]["lineNumber"] = lineNumber;
  editors[editorId]["cursorX"] = cursorX;
  
  let parinferResult = parinferFunc(text, options);
    
  if (parinferResult.success && parinferResult.text !== text) {
    editors[editorId]["text"] = parinferResult.text;
    
    editor.edit(edits => {
      console.log("Parinfer is editing document…")
      edits.replace(range, parinferResult.text);
    });
    
    let documentTextRange = new Range(0, document.length);
    let documentText = document.getTextInRange(documentTextRange);
    
    console.log("Current cursor pos:" + editor.selectedRange.start);
    
    let newPos = colAndCursorXtoPos(parinferResult.cursorLine, parinferResult.cursorX, document.eol, documentText);
    let newRange = new Range(newPos, newPos);
    editor.selectedRange = newRange;
  } else {
    editors[editorId]["text"] = text;
  }
}


function onNewEditor(editor) {
  let syntax = editor.document.syntax;
  
  if (syntax === "clj"  ||
      syntax === "cljs" ||
      syntax === "cljc" ||
      syntax === "edn") {
        let { text, cursorX, lineNumber } = getDocumentTextAndRange(editor.selectedRange, editor.document); 
        
        let editorId = uuidv4();
        
        editors[editorId] = {
          text: text,
          cursorX: cursorX,
          lineNumber: lineNumber
        };
        
        editor.onDidDestroy(editor => {
          delete editors[editorId];
        });
        
        editor.onDidChangeSelection(editor => {
          let { cursorX, lineNumber } = getDocumentTextAndRange(editor.selectedRange, editor.document);
          
          // Hack around the fact that selection updates will feed the wrong info to 
          // Parinfer. Yet must stay up-to-date while navigating around doc.
          setTimeout(() => { 
            editors[editorId]["cursorX"] = cursorX;
            editors[editorId]["lineNumber"] = lineNumber;
            
            console.log("Updating selection, line: " + lineNumber + ", pos: " + cursorX);
          }, 100);
        })
        
        console.log("Editors");
        console.log(JSON.stringify(editors));
        
        runParinferOnDocument('paren', editor, editorId);
        
        editor.onWillSave(editor => {
          runParinferOnDocument('paren', editor, editorId);
        });
        
        editor.onDidChange(editor => {
          runParinferOnDocument('smart', editor, editorId);
        });
      }
}


exports.activate = function() {
    // Do work when the extension is activated
    console.log("Activating Parinfer");
    nova.workspace.onDidAddTextEditor(onNewEditor);
}

exports.deactivate = function() {
    // Clean up state before the extension is deactivated
    console.log("Deactivating Parinfer")
}

