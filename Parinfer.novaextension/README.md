# Parinfer-Nova

This is an experimental [Parinfer](https://shaunlebron.github.io/parinfer/) plugin for Nova, based on [parinfer-rust](https://github.com/eraserhd/parinfer-rust). It has many, many flaws, and should be considered unstable.

## Highlighted features
- **Inefficient**: it runs several times per edit (due to lack of hooks into edits in flight; an edit triggers Parinfer, which triggers another edit, which triggers Parinfer, finally terminating when Parinfer emits no further edits).
- **Makes your undo stack unusable**: Parinfer's adjustments are inserted in the undo stack as a separate edit. Running an undo counts as an edit, which re-runs Parinfer.
- **Unpredictable behaviour**: Parinfer needs to be fed heuristics such as the text, line number, and cursor position before and after an edit. None of these are readily retrievable from Nova in context of an edit, and my workarounds for getting them to work are only correct in the ballpark sense of the word.
- **A lot of debug logging**: Prepare to see a lot of exciting stuff in your extension console!
